//
//  SeptaLineTests.swift
//  R2Newark
//
//  Created by Thom Loring on 6/28/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

import XCTest
import R2Newark

class SeptaLine2Tests: XCTestCase {

    var septaLine2 = SeptaLine2()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSeptaLine() {
        // This is an example of a functional test case.
        XCTAssertNotNil(self.septaLine2, "Object Not Nil")
        XCTAssertTrue(self.septaLine2.stationCount == 22, "Station Count")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }

}

// http://stackoverflow.com/questions/24029781/how-do-i-import-a-swift-file-from-another-swift-file