//
//  SeptaData.m
//  R2Newark
//
//  Created by Xcode on 2/1/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

#import "SeptaData.h"
#import "SeptaStation.h"
#import "AFNetworking/AFNetworking.h"

@interface SeptaData ()
@end

@implementation SeptaData

+ (instancetype)sharedSeptaData
{
    static SeptaData *_sharedSeptaData = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedSeptaData = [[self alloc] init];
        NSLog(@"%s shared SeptaData singleton created", __PRETTY_FUNCTION__);
    });
    
    return _sharedSeptaData;
}

- (instancetype)init
{
    if ( self = [super init]) {
        //[self fetchStations];
    }
    return self;
}

- (void)fetchStations:(void (^)())doneBlock;
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager POST:@"http://r2septa.herokuapp.com/stations" parameters:@{}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSLog(@"%s JSON: %@", __PRETTY_FUNCTION__, responseObject);
              NSLog(@"%s %@", __PRETTY_FUNCTION__, [responseObject class]);
              NSLog(@"%s %@", __PRETTY_FUNCTION__, responseObject[0]);
              self.stations = [[responseObject reverseObjectEnumerator] allObjects];
              if ( doneBlock ) doneBlock();
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"%s Error: %@", __PRETTY_FUNCTION__, error);
          }
     ];
}

- (void)fetchSchedule:(NSDictionary *)parameters
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager POST:@"http://r2septa.herokuapp.com"
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                   NSLog(@"JSON: %@", responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   NSLog(@"Error: %@", error);
        }
    ];
}

- (void)fetchScheduleFrom:(SeptaStation *)from to:(SeptaStation *)to
{
    NSDictionary *parameters = @{
                                 @"orig_name": from.name,
                                 @"orig_tminus": from.tminus,
                                 @"orig_tplus": from.tplus,
                                 @"dest_name": to.name,
                                 @"dest_tminus": to.tminus,
                                 @"dest_tplus" : to.tplus
                                 };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager POST:@"http://r2septa.herokuapp.com"
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                   NSLog(@"JSON: %@", responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   NSLog(@"Error: %@", error);
        }
     ];
}

@end
