//
//  SeptaStation.m
//  R2Newark
//
//  Created by Xcode on 2/1/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

#import "SeptaStation.h"

@implementation SeptaStation

- (instancetype)initWithName:(NSString *)name latitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude
{
    NSLog(@"%s %@, %f, %f", __PRETTY_FUNCTION__, name, latitude, longitude);
    
    if ( (self = [super init]) ) {
        self.name = name;
        self.latitude = latitude;
        self.longitude = longitude;
        self.location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        self.centerCoordinate = CLLocationCoordinate2DMake(latitude, longitude);
        self.regionRadius = 10.0;
        self.region = [[CLCircularRegion alloc] initWithCenter:self.centerCoordinate
                                                        radius:self.regionRadius
                                                    identifier:self.name];
    }
    
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ at (%f,%f) %@", self.name, self.latitude, self.longitude, self.location];
}

@end
