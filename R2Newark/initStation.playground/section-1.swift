// Playground - noun: a place where people can play

import CoreLocation

let x = ["name":"Temple University",             "latitude":"39.9816",   "longitude":"75.1495"]

let y = ["name":"Temple University",             "latitude":"39.9816",   "longitude":"75.1495"]


var station_coords: Dictionary<String,String>[] = [
    ["name":"Newark",                        "latitude":"39.670278", "longitude":"-75.753056"],
    ["name":"Churchman's Crossing",          "latitude":"39.6940",   "longitude":"-75.6724"],
    ["name":"Wilmington",                    "latitude":"39.736667", "longitude":"-75.551667"],
    ["name":"Claymont",                      "latitude":"39.7976",   "longitude":"-75.4521"],
    ["name":"Marcus Hook",                   "latitude":"39.8215",   "longitude":"-75.4197"],
    ["name":"Highland Avenue",               "latitude":"39.8337",   "longitude":"-75.3929"],
    ["name":"Chester Transportation Center", "latitude":"39.84932",  "longitude":"-75.35988"],
    ["name":"Eddystone",                     "latitude":"39.8573",   "longitude":"-75.3416"],
    ["name":"Crum Lynne",                    "latitude":"39.8719",   "longitude":"-75.3311"],
    ["name":"Ridley Park",                   "latitude":"39.880523", "longitude":"-75.322105"],
    ["name":"Prospect Park",                 "latitude":"39.888114", "longitude":"-75.309434"],
    ["name":"Norwood",                       "latitude":"39.891360", "longitude":"-75.302221"],
    ["name":"Glenolden",                     "latitude":"39.896362", "longitude":"-75.289854"],
    ["name":"Folcroft",                      "latitude":"39.900667", "longitude":"-75.279543"],
    ["name":"Sharon Hill",                   "latitude":"39.904255", "longitude":"-75.270971"],
    ["name":"Curtis Park",                   "latitude":"39.908083", "longitude":"-75.265008"],
    ["name":"Darby",                         "latitude":"39.912962", "longitude":"-75.254588"],
    ["name":"University City",               "latitude":"39.94784",  "longitude":"-75.19034"],
    ["name":"30th Street Station",           "latitude":"39.956924", "longitude":"-75.182576"],
    ["name":"Suburban Station",              "latitude":"39.954167", "longitude":"-75.167"],
    ["name":"Market East",                   "latitude":"39.952076", "longitude":"-75.156612"],
    ["name":"Temple University",             "latitude":"39.9816",   "longitude":"-75.1495"]
]
println(station_coords)

let coord = station_coords[0]

coord["name"]
let s = NSString(string:coord["longitude"]).doubleValue

let station_coords2: Dictionary<String,Any>[] = [
    ["name":"Newark",                        "latitude":39.670278, "longitude":-75.753056],
    ["name":"Churchman's Crossing",          "latitude":39.6940,   "longitude":-75.6724],
    ["name":"Wilmington",                    "latitude":39.736667, "longitude":-75.551667],
    ["name":"Claymont",                      "latitude":39.7976,   "longitude":-75.4521],
    ["name":"Marcus Hook",                   "latitude":39.8215,   "longitude":-75.4197],
    ["name":"Highland Avenue",               "latitude":39.8337,   "longitude":-75.3929],
    ["name":"Chester Transportation Center", "latitude":39.84932,  "longitude":-75.35988],
    ["name":"Eddystone",                     "latitude":39.8573,   "longitude":-75.3416],
    ["name":"Crum Lynne",                    "latitude":39.8719,   "longitude":-75.3311],
    ["name":"Ridley Park",                   "latitude":39.880523, "longitude":-75.322105],
    ["name":"Prospect Park",                 "latitude":39.888114, "longitude":-75.309434],
    ["name":"Norwood",                       "latitude":39.891360, "longitude":-75.302221],
    ["name":"Glenolden",                     "latitude":39.896362, "longitude":-75.289854],
    ["name":"Folcroft",                      "latitude":39.900667, "longitude":-75.279543],
    ["name":"Sharon Hill",                   "latitude":39.904255, "longitude":-75.270971],
    ["name":"Curtis Park",                   "latitude":39.908083, "longitude":-75.265008],
    ["name":"Darby",                         "latitude":39.912962, "longitude":-75.254588],
    ["name":"University City",               "latitude":39.94784,  "longitude":-75.19034],
    ["name":"30th Street Station",           "latitude":39.956924, "longitude":-75.182576],
    ["name":"Suburban Station",              "latitude":39.954167, "longitude":-75.167],
    ["name":"Market East",                   "latitude":39.952076, "longitude":-75.156612],
    ["name":"Temple University",             "latitude":39.9816,   "longitude":-75.1495]
]
println(station_coords2)

let coord2 = station_coords2[0]
coord2["longitude"]

for coord_dict in station_coords2 {
    println(coord_dict)
    println(coord_dict["name"])
    println(coord_dict["latitude"])
    println(coord_dict["longitude"])
}

for (var index=0; index<station_coords2.count; index++) {
    println(station_coords2[index])
}
