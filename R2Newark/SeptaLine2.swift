//
//  SeptaRoute.m
//  R2Newark
//
//  Created by Thom Loring on 2/19/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

import CoreLocation
import AudioToolbox

class SeptaLine2: NSObject, CLLocationManagerDelegate
{
    // Inbound/Outbound Stations
    var homeStation:SeptaStation? = nil // ie, Claymont  TODO: recalc total_distance on assign
    var cityStation:SeptaStation? = nil // ie, 30th Street TODO: recalc total_distance on assign
    var total_distance:Double = 0
    var sub_stations:[SeptaStation] = []
    var mile_markers:[Double] = []
    
    var stations: Array<SeptaStation> = []
    var stationCount: Int = 0
    var locationManager: CLLocationManager = CLLocationManager()
    // var speed_mph: Double
    // var direction_string: String
    // var last_modified: NSDate
    
    var timestamp:NSDate? = nil
    var location:CLLocation? = nil { // Current Location on the Line
      // Setting Current Location updates the Station Objects distance from current location
      willSet(newLocation) {
        for index in 0..<self.stations.count {
            self.stations[index].distance = newLocation!.distanceFromLocation(self.stations[index].location)
        }
      }
    }
    
    override init()
    {
        super.init()
        initStationData()
        initCoreLocation()
    }
    
    convenience init(homeStation:String, cityStation:String)
    {
        self.init()
        
        self.homeStation = self[homeStation]
        self.cityStation = self[cityStation]
        self.total_distance = milesBetweenStations(self.homeStation!, and: self.cityStation!)
        self.sub_stations = stationsFrom(self.homeStation!, to: self.cityStation!)
        println("Stations of Interest: \(self.sub_stations)")
        
        for station in sub_stations {
            self.mile_markers += [milesBetweenStations(self.homeStation!, and:station)]
        }
        println("Mile Markers: \(self.mile_markers)")
    }
    
    func initStationData()
    {
        struct Station {
            let name: String
            let latitude: CLLocationDegrees
            let longitude: CLLocationDegrees
        }
        
        let station_coords = [
            Station(name:"Newark",                        latitude:39.670278, longitude:-75.753056),
            Station(name:"Churchman's Crossing",          latitude:39.6940,   longitude:-75.6724),
            Station(name:"Wilmington",                    latitude:39.736667, longitude:-75.551667),
            Station(name:"Claymont",                      latitude:39.7976,   longitude:-75.4521),
            Station(name:"Marcus Hook",                   latitude:39.8215,   longitude:-75.4197),
            Station(name:"Highland Avenue",               latitude:39.8337,   longitude:-75.3929),
            Station(name:"Chester Transportation Center", latitude:39.84932,  longitude:-75.35988),
            Station(name:"Eddystone",                     latitude:39.8573,   longitude:-75.3416),
            Station(name:"Crum Lynne",                    latitude:39.8719,   longitude:-75.3311),
            Station(name:"Ridley Park",                   latitude:39.880523, longitude:-75.322105),
            Station(name:"Prospect Park",                 latitude:39.888114, longitude:-75.309434),
            Station(name:"Norwood",                       latitude:39.891360, longitude:-75.302221),
            Station(name:"Glenolden",                     latitude:39.896362, longitude:-75.289854),
            Station(name:"Folcroft",                      latitude:39.900667, longitude:-75.279543),
            Station(name:"Sharon Hill",                   latitude:39.904255, longitude:-75.270971),
            Station(name:"Curtis Park",                   latitude:39.908083, longitude:-75.265008),
            Station(name:"Darby",                         latitude:39.912962, longitude:-75.254588),
            Station(name:"University City",               latitude:39.94784,  longitude:-75.19034),
            Station(name:"30th Street Station",           latitude:39.956924, longitude:-75.182576),
            Station(name:"Suburban Station",              latitude:39.954167, longitude:-75.167),
            Station(name:"Market East",                   latitude:39.952076, longitude:-75.156612),
            Station(name:"Temple University",             latitude:39.9816,   longitude:-75.1495)
        ]
        
        var index = 0
        for station_coord in station_coords {
            var station = SeptaStation(name: station_coord.name, latitude:station_coord.latitude, longitude:station_coord.longitude)
            station.index = index++
            self.stations += [station]
        }
        self.stationCount = station_coords.count
        
        for index in 0..<self.stationCount-1 {
            var stationA = self.stations[index]
            var stationB = self.stations[index+1]
            var a:CLLocation = stationA.location
            var b:CLLocation = stationB.location
            stationA.milesNextStationNorth = a.distanceFromLocation(b) * 0.000621371
        }
        
        func generateGPX()
        {
            var output = "\n"
            output += "<?xml version='1.0'?>\n"
            output += "<gpx version='1.1' creator='Xcode'>\n"
            
            for station in self.stations {
                output += "  <wpt lat='\(station.latitude)' lon='\(station.longitude)'>\n"
                output += "    <name>\(station.name)</name>\n"
                output += "  </wpt>\n"
            }
            
            output += "</gpx>\n"
            println(output)
        }
    
        generateGPX()
    }
    
    func initCoreLocation()
    {
        if !CLLocationManager.locationServicesEnabled() {
            println("Need to enable Location Services")
        }
        
        if !CLLocationManager.isMonitoringAvailableForClass(nil/*CLRegion.class()*/) {
            println("Region Monitoring is not available")
        }
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Denied ||
           CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Restricted
        {
            println("You need to authorize Location Services for the App")
        }
        
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.distanceFilter = 5.0
        locationManager.delegate = self
        locationManager.pausesLocationUpdatesAutomatically = true
        locationManager.startMonitoringSignificantLocationChanges()
        
        for station in self.stations {
            locationManager.startMonitoringForRegion(station.region)
        }
        
        println("initCoreLocation: \(locationManager.monitoredRegions.allObjects)")
        
        locationManager.startUpdatingHeading()
    }
    


    //
    // Subscript Convenience Methods
    //
    // septaLine["Claymont"] returns Claymont Station Object
    // septaLine[3] returns 4th station from South
    //
    
    subscript(key:String) -> SeptaStation? {
        get {
            for station in stations {
                if station.name == key {
                    return station
                }
            }
            return nil
        }
    }
    
    subscript(index:Int) -> SeptaStation {
        get {
            return self.stations[min(index, stationCount-1)]
        }
    }
    
    // Utility Methods
    
    func stationsFrom(a:SeptaStation, to b:SeptaStation) ->  Array<SeptaStation>
    {
        var first_station_seen = false
        var result_array:Array<SeptaStation> = []
        for station in stations {
            if !first_station_seen && station.name != a.name {
                continue
            } else {
                first_station_seen = true
                result_array += [station]
                if station.name == b.name {
                    break
                }
            }
        }
        return result_array
    }
    
    func milesBetweenStations(a:SeptaStation, and b:SeptaStation) -> Double
    {
       return a.location.distanceFromLocation(b.location) * 0.000621371
    }
}

/*

#pragma mark - Current Location Methods

//
// Methods relating to Current Location
//
// atStation
// closestStation
// betweenStations
// southOfStation
// northOfStation
// milesFrom
//

- (SeptaStation *)atStation
{
    for (SeptaStation *station in self.stations) {
        if ( station.distance < 100 ) {
            NSLog(@"%s %@", __PRETTY_FUNCTION__, station.name);
            return station;
        }
    }
    return nil;
}

-(SeptaStation *)closestStation
{
    double min_distance = MAXFLOAT;
    SeptaStation *min_station;
    
    for (SeptaStation *station in self.stations) {
        if ( station.distance < min_distance ) {
            min_distance = station.distance;
            min_station = station;
        }
    }
    
    NSLog(@"%s %@ %f", __PRETTY_FUNCTION__, min_station.name, min_distance);
    return (min_station);
}

-(NSArray *)betweenStations
{
    for (int index=0; index < self.stations.count-1; index++) {
        SeptaStation *stationA = self.stations[index];
        SeptaStation *stationB = self.stations[index+1];
        CLLocation *a = stationA.location;
        CLLocation *b = stationB.location;
        CLLocation *c = self.location;
        if ( c.coordinate.latitude > a.coordinate.latitude && c.coordinate.latitude < b.coordinate.latitude) {
            if ( c.coordinate.longitude > a.coordinate.longitude  && c.coordinate.longitude < b.coordinate.longitude) {
                NSLog(@"%s Between %@ and %@", __PRETTY_FUNCTION__, stationA.name, stationB.name);
                return @[stationB, stationA]; // order N->S
                /*
                // returning textually the order based on current location course
                if ( (self.location.course >= 0 && self.location.course < 90) || (self.location.course >= 270 && self.location.course < 360) )
                return @[stationB, stationA];
                else
                return @[stationA, stationB];
                */
            }
        }
    }
    return nil;
}

-(BOOL)southOfStation:(SeptaStation *)station
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"%s location: %@", __PRETTY_FUNCTION__, self.location);
    NSLog(@"%s station:  %@", __PRETTY_FUNCTION__, station.location);
    return self.location.coordinate.latitude < station.location.coordinate.latitude;
}

-(BOOL)northOfStation:(SeptaStation *)station
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"%s location: %@", __PRETTY_FUNCTION__, self.location);
    NSLog(@"%s station:  %@", __PRETTY_FUNCTION__, station.location);
    return self.location.coordinate.latitude > station.location.coordinate.latitude;
}

-(double)milesFrom:(SeptaStation *)station
{
    return station.distance  * 0.000621371;
}

#pragma mark - Other Location Methods

//
// Other Location Methods
//
// milesBetweenStations
//

#pragma mark - Utility Methods

//
// Utility Methods
//

-(NSString *)direction_string:(CLLocationDirection)direction
{
    if ( direction < 0 ) return @"\u00D8";
    if ( direction >=   0 && direction <=  22 ) return @"\u2191"; // North
    if ( direction >=  23 && direction <=  67 ) return @"\u2197"; // NorthEast
    if ( direction >=  68 && direction <= 112 ) return @"\u2192"; // East
    if ( direction >= 113 && direction <= 157 ) return @"\u2198"; // SouthEast
    if ( direction >= 158 && direction <= 180 ) return @"\u2193"; // South
    if ( direction >= 181 && direction <= 202 ) return @"\u2193"; // South
    if ( direction >= 203 && direction <= 247 ) return @"\u2199"; // SouthWest
    if ( direction >= 248 && direction <= 292 ) return @"\u2190"; // West
    if ( direction >= 293 && direction <= 337 ) return @"\u2196"; // NorthWest
    if ( direction >= 338 && direction <= 360 ) return @"\u2191"; // North
    return @"\u00B7";
}

#pragma mark - CoreLocation Delegates

//
// CoreLocation Delegates, on update of location, call this object's delegate septaLineLocationUpdate
//

- (void)locationManager:(CLLocationManager *)manager
didFailWithError:(NSError *)error
{
    NSLog(@"%s Error: %@", __PRETTY_FUNCTION__, [error description]);
    
    /*
    Error Domain=kCLErrorDomain Code=0 "The operation couldn’t be completed. (kCLErrorDomain error 0.)"
    This error also occurs if you have Scheme/Edit Scheme/Options/Allow Location Simulation
    checked but don't have a default location set. Also, unset that to run on a device!
    */
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%s %@", __PRETTY_FUNCTION__, [locations.lastObject description]);
    
    self.location = locations.lastObject;
    
    NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:@"coord(%.3f,%.3f)",
    self.location.coordinate.latitude, self.location.coordinate.longitude]);
    
    self.timestamp = [NSDate dateWithTimeInterval:[[NSTimeZone localTimeZone] secondsFromGMT]
    sinceDate:self.location.timestamp];
    
    self.speed_mph = MAX(self.location.speed * 2.23693629, 0);
    self.direction_string = [self direction_string:self.location.course];
    
    [self.delegate septaLineLocationUpdate:self];
    
    SeptaStation *at_station;
    NSArray *stations;
    
    if ( (at_station = [self atStation]) )
    {
        [self.delegate septaLineLocationAtStation:at_station];
        
        if ( !self.last_notified || [self.last_notified timeIntervalSinceNow] < -180  ) {
            NSLog(@"%s notifying", __PRETTY_FUNCTION__);
            //UILocalNotification *notify = [[UILocalNotification alloc] init];
            //notify.alertBody = [NSString stringWithFormat:@"At %@", at_station.name];
            //[[UIApplication sharedApplication] presentLocalNotificationNow:notify];
            self.last_notified = [NSDate date];
        } else {
            NSLog(@"%s throttling notifications %@ %f", __PRETTY_FUNCTION__,
            self.last_notified, [self.last_notified timeIntervalSinceNow]);
        }
    }
    else if ( (stations = [self betweenStations]) )
    {
        [self.delegate septaLineLocationBetweenStations:stations[0] and:stations[1]];
    }
    else
    {
        [self.delegate septaLineLocationClosestStation:[self closestStation]];
    }
}

#pragma mark - Geofencing Delegate Methods

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    NSLog(@"%s %@", __PRETTY_FUNCTION__, region);
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    UILocalNotification *notify = [[UILocalNotification alloc] init];
    notify.alertBody = [NSString stringWithFormat:@"Arriving %@", region.identifier];
    notify.soundName = @"silence.aif";
    [[UIApplication sharedApplication] presentLocalNotificationNow:notify];
    }
    
    - (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    NSLog(@"%s %@", __PRETTY_FUNCTION__, region);
    //UILocalNotification *notify = [[UILocalNotification alloc] init];
    //notify.alertBody = [NSString stringWithFormat:@"Departing %@", region.identifier];
    //[[UIApplication sharedApplication] presentLocalNotificationNow:notify];
    }
    - (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    NSLog(@"%s %@", __PRETTY_FUNCTION__, region);
    // UILocalNotification *notify = [[UILocalNotification alloc] init];
    // notify.alertBody = [NSString stringWithFormat:@"Monitoring %@", region.identifier];
    // [[UIApplication sharedApplication] presentLocalNotificationNow:notify];
    }
    
    - (void)locationManager:(CLLocationManager *)manager
didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    NSLog(@"%s %d %@", __PRETTY_FUNCTION__, state, region);
}

@end
*/