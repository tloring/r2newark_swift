//
//  SeptaRoute.m
//  R2Newark
//
//  Created by Thom Loring on 2/19/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

#import "SeptaLine.h"
@import CoreLocation;
@import AudioToolbox;

#import "R2Newark-Swift.h"

@interface SeptaLine () <CLLocationManagerDelegate>

@property (nonatomic) NSMutableArray *stations;
@property (nonatomic) NSUInteger stationCount;    // public readonly
@property (nonatomic) double speed_mph;           // public readonly
@property (nonatomic) NSString *direction_string; // public readonly
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) NSDate *last_notified;

@end

@implementation SeptaLine

#pragma mark - Initializers

//
// Initialize Object
//

- (instancetype)init
{
    self = [super init];
    
    if (self != nil) {
        [self initStationData];
        [self initCoreLocation];
    }
    
    return self;
}

- (instancetype)initWithHomeStation:(NSString *)origin cityStation:(NSString *)city
{
    self = [self init];
    
    if (self != nil) {
        self.homeStation = self[origin];
        self.cityStation = self[city];
        self.total_distance = [self milesBetweenStations:self.homeStation and:self.cityStation];
    
        self.sub_stations = [self stationsFrom:self.homeStation to:self.cityStation];
        NSLog(@"%s stations of interest %@", __PRETTY_FUNCTION__, self.sub_stations);
    
        NSMutableArray *mile_markers = [[NSMutableArray alloc] init];
        for (SeptaStation *station in self.sub_stations) {
            [mile_markers addObject:@([self milesBetweenStations:self.homeStation and:station])];
        }
        self.mile_markers = mile_markers;
        NSLog(@"%s mile_markers %@", __PRETTY_FUNCTION__, mile_markers);
    }
    
    return self;
}

//
// Initialize Station Data
//

-(void)initStationData
{
    // Stations, Coordinates South -> North
    
    NSArray *station_coords = @[
      @{ @"name":@"Newark",                        @"latitude":@39.670278, @"longitude":@-75.753056},
      @{ @"name":@"Churchman's Crossing",          @"latitude":@39.6940,   @"longitude":@-75.6724},
      @{ @"name":@"Wilmington",                    @"latitude":@39.736667, @"longitude":@-75.551667},
      @{ @"name":@"Claymont",                      @"latitude":@39.7976,   @"longitude":@-75.4521},
      @{ @"name":@"Marcus Hook",                   @"latitude":@39.8215,   @"longitude":@-75.4197},
      @{ @"name":@"Highland Avenue",               @"latitude":@39.8337,   @"longitude":@-75.3929},
      @{ @"name":@"Chester Transportation Center", @"latitude":@39.84932,  @"longitude":@-75.35988},
      @{ @"name":@"Eddystone",                     @"latitude":@39.8573,   @"longitude":@-75.3416},
      @{ @"name":@"Crum Lynne",                    @"latitude":@39.8719,   @"longitude":@-75.3311},
      @{ @"name":@"Ridley Park",                   @"latitude":@39.880523, @"longitude":@-75.322105},
      @{ @"name":@"Prospect Park",                 @"latitude":@39.888114, @"longitude":@-75.309434},
      @{ @"name":@"Norwood",                       @"latitude":@39.891360, @"longitude":@-75.302221},
      @{ @"name":@"Glenolden",                     @"latitude":@39.896362, @"longitude":@-75.289854},
      @{ @"name":@"Folcroft",                      @"latitude":@39.900667, @"longitude":@-75.279543},
      @{ @"name":@"Sharon Hill",                   @"latitude":@39.904255, @"longitude":@-75.270971},
      @{ @"name":@"Curtis Park",                   @"latitude":@39.908083, @"longitude":@-75.265008},
      @{ @"name":@"Darby",                         @"latitude":@39.912962, @"longitude":@-75.254588},
      @{ @"name":@"University City",               @"latitude":@39.94784,  @"longitude":@-75.19034},
      @{ @"name":@"30th Street Station",           @"latitude":@39.956924, @"longitude":@-75.182576},
      @{ @"name":@"Suburban Station",              @"latitude":@39.954167, @"longitude":@-75.167},
      @{ @"name":@"Market East",                   @"latitude":@39.952076, @"longitude":@-75.156612},
      @{ @"name":@"Temple University",             @"latitude":@39.9816,   @"longitude":@-75.1495}
    ];
    NSLog(@"%s station_coords %@", __PRETTY_FUNCTION__, station_coords);
    
    // Array of Station Objects, w/ CoreLocation
    
    self.stations = [[NSMutableArray alloc] init];
    
    NSInteger index=0;
    for (NSDictionary *coords in station_coords) {
        SeptaStation *station = [[SeptaStation alloc] initWithName:coords[@"name"]
                                                          latitude:[coords[@"latitude"] doubleValue]
                                                         longitude:[coords[@"longitude"] doubleValue]];
        station.index = index++;
        [self.stations addObject:station];
    }
    NSLog(@"%s Stations Array: %@", __PRETTY_FUNCTION__, self.stations);
    self.stationCount = station_coords.count;

    // Distances between Stations heading north (in miles)
    
    for (int index=0; index < self.stations.count-1; index++) {
        SeptaStation *stationA = self.stations[index];
        SeptaStation *stationB = self.stations[index+1];
        CLLocation *a = stationA.location;
        CLLocation *b = stationB.location;
        stationA.milesNextStationNorth = [a distanceFromLocation:b] * 0.000621371;
    }
    
    // [self generateGPX]; // for testing
}

-(void)generateGPX
{
    NSString *output = @"\n";
    output = [output stringByAppendingString:@"<?xml version='1.0'?>\n"];
    output = [output stringByAppendingString:@"<gpx version='1.1' creator='Xcode'>\n"];
    /*
     <wpt lat="41.88535" lon="-87.62745">
     <name>Fence 1</name>
     </wpt>
    */
    for (SeptaStation *station in self.stations) {
        output = [output stringByAppendingFormat:@"  <wpt lat='%f' lon='%f'>\n", station.latitude, station.longitude];
        output = [output stringByAppendingFormat:@"    <name>%@</name>\n", station.name];
        output = [output stringByAppendingFormat:@"  </wpt>\n"];
    }
    output = [output stringByAppendingString:@"</gpx>\n"];
    NSLog(@"%s %@", __PRETTY_FUNCTION__, output);
}

//
// Core Location; CoreLocation deletages at end of file
//

-(void)initCoreLocation
{
    if(![CLLocationManager locationServicesEnabled])
    {
        NSLog(@"%s %@", __PRETTY_FUNCTION__, @"You need to enable Location Services");
    }
    if(![CLLocationManager isMonitoringAvailableForClass:[CLRegion class]])
    {
        NSLog(@"%s %@", __PRETTY_FUNCTION__, @"Region monitoring is not available for this Class");
    }
    if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied ||
       [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted  )
    {
        NSLog(@"%s %@", __PRETTY_FUNCTION__, @"You need to authorize Location Services for the APP");
    }
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    self.locationManager.distanceFilter = 5.0;
    self.locationManager.delegate = self;
    self.locationManager.pausesLocationUpdatesAutomatically = YES;
    //[self.locationManager startMonitoringSignificantLocationChanges];
    
    for (SeptaStation *station in self.stations) {
      [self.locationManager startMonitoringForRegion:station.region];
    }
    
    NSLog(@"%s %@", __PRETTY_FUNCTION__, [self.locationManager.monitoredRegions allObjects]);
    
    [self.locationManager startUpdatingLocation];
}

#pragma mark - Subscript Keys/Indices

//
// Subscript Convenience Methods
//
// septaLine[@"Claymont"] returns Claymont Station Object
// septaLine[3] returns 4th station from South
//

-(id)objectForKeyedSubscript:(id<NSCopying>)key
{
    for (SeptaStation *station in self.stations) {
        if ( station.name == key ) {
            return station;
        }
    }
    return nil;
}

-(id)objectAtIndexedSubscript:(NSUInteger)index
{
    return self.stations[MIN(index, self.stationCount-1)];
}

#pragma mark - Property Methods

//
// Setting Current Location updates the Station Objects distance from current location
//

-(void)setLocation:(CLLocation *)location
{
    NSLog(@"%s %@", __PRETTY_FUNCTION__, location);
    _location = location;
    for (int index=0; index < self.stations.count; index++) {
        SeptaStation *station = (SeptaStation *)self.stations[index];
        station.distance = [_location distanceFromLocation:station.location];
    }
}

#pragma mark - Current Location Methods

//
// Methods relating to Current Location
//
// atStation
// closestStation
// betweenStations
// southOfStation
// northOfStation
// milesFrom
//

- (SeptaStation *)atStation
{
    for (SeptaStation *station in self.stations) {
        if ( station.distance < 100 ) {
            NSLog(@"%s %@", __PRETTY_FUNCTION__, station.name);
            return station;
        }
    }
    return nil;
}

-(SeptaStation *)closestStation
{
    double min_distance = MAXFLOAT;
    SeptaStation *min_station;
    
    for (SeptaStation *station in self.stations) {
        if ( station.distance < min_distance ) {
            min_distance = station.distance;
            min_station = station;
        }
    }
    
    NSLog(@"%s %@ %f", __PRETTY_FUNCTION__, min_station.name, min_distance);
    return (min_station);
}

-(NSArray *)betweenStations
{
    for (int index=0; index < self.stations.count-1; index++) {
        SeptaStation *stationA = self.stations[index];
        SeptaStation *stationB = self.stations[index+1];
        CLLocation *a = stationA.location;
        CLLocation *b = stationB.location;
        CLLocation *c = self.location;
        if ( c.coordinate.latitude > a.coordinate.latitude && c.coordinate.latitude < b.coordinate.latitude) {
            if ( c.coordinate.longitude > a.coordinate.longitude  && c.coordinate.longitude < b.coordinate.longitude) {
                NSLog(@"%s Between %@ and %@", __PRETTY_FUNCTION__, stationA.name, stationB.name);
                return @[stationB, stationA]; // order N->S
                /* 
                // returning textually the order based on current location course
                if ( (self.location.course >= 0 && self.location.course < 90) || (self.location.course >= 270 && self.location.course < 360) )
                    return @[stationB, stationA];
                else
                    return @[stationA, stationB];
                */
            }
        }
    }
    return nil;
}

-(BOOL)southOfStation:(SeptaStation *)station
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"%s location: %@", __PRETTY_FUNCTION__, self.location);
    NSLog(@"%s station:  %@", __PRETTY_FUNCTION__, station.location);
    return self.location.coordinate.latitude < station.location.coordinate.latitude;
}

-(BOOL)northOfStation:(SeptaStation *)station
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"%s location: %@", __PRETTY_FUNCTION__, self.location);
    NSLog(@"%s station:  %@", __PRETTY_FUNCTION__, station.location);
    return self.location.coordinate.latitude > station.location.coordinate.latitude;
}

-(double)milesFrom:(SeptaStation *)station
{
    return station.distance  * 0.000621371;
}

#pragma mark - Other Location Methods

//
// Other Location Methods
//
// milesBetweenStations
//

-(NSArray *)stationsFrom:(SeptaStation *)a to:(SeptaStation *)b
{
    NSLog(@"%s from:%@ to:%@", __PRETTY_FUNCTION__, a, b);
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    BOOL first_station_seen = NO;
    for (SeptaStation *station in self.stations) {
        NSLog(@"%s %@", __PRETTY_FUNCTION__, station);
        if ( !first_station_seen && station.name != a.name ) {
            continue;
        } else {
            first_station_seen = YES;
            [resultArray addObject:station];
            if ( station.name == b.name ) {
               break;
            }
        }
    }
    return resultArray;
}

-(double)milesBetweenStations:(SeptaStation *)a and:(SeptaStation *)b
{
    return [a.location distanceFromLocation:b.location] * 0.000621371;
}

#pragma mark - Utility Methods

//
// Utility Methods
//

-(NSString *)direction_string:(CLLocationDirection)direction
{
    if ( direction < 0 ) return @"\u00D8";
    if ( direction >=   0 && direction <=  22 ) return @"\u2191"; // North
    if ( direction >=  23 && direction <=  67 ) return @"\u2197"; // NorthEast
    if ( direction >=  68 && direction <= 112 ) return @"\u2192"; // East
    if ( direction >= 113 && direction <= 157 ) return @"\u2198"; // SouthEast
    if ( direction >= 158 && direction <= 180 ) return @"\u2193"; // South
    if ( direction >= 181 && direction <= 202 ) return @"\u2193"; // South
    if ( direction >= 203 && direction <= 247 ) return @"\u2199"; // SouthWest
    if ( direction >= 248 && direction <= 292 ) return @"\u2190"; // West
    if ( direction >= 293 && direction <= 337 ) return @"\u2196"; // NorthWest
    if ( direction >= 338 && direction <= 360 ) return @"\u2191"; // North
    return @"\u00B7";
}

#pragma mark - CoreLocation Delegates

//
// CoreLocation Delegates, on update of location, call this object's delegate septaLineLocationUpdate
//

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
	NSLog(@"%s Error: %@", __PRETTY_FUNCTION__, [error description]);
    
    /*
     Error Domain=kCLErrorDomain Code=0 "The operation couldn’t be completed. (kCLErrorDomain error 0.)"
     This error also occurs if you have Scheme/Edit Scheme/Options/Allow Location Simulation
     checked but don't have a default location set. Also, unset that to run on a device!
     */
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%s %@", __PRETTY_FUNCTION__, [locations.lastObject description]);
    
    self.location = locations.lastObject;
    
    NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:@"coord(%.3f,%.3f)",
                                          self.location.coordinate.latitude, self.location.coordinate.longitude]);
    
    self.timestamp = [NSDate dateWithTimeInterval:[[NSTimeZone localTimeZone] secondsFromGMT]
                                        sinceDate:self.location.timestamp];
    
    self.speed_mph = MAX(self.location.speed * 2.23693629, 0);
    self.direction_string = [self direction_string:self.location.course];
    
    [self.delegate septaLineLocationUpdate:self];
    
    SeptaStation *at_station;
    NSArray *stations;
    
    if ( (at_station = [self atStation]) )
    {
        [self.delegate septaLineLocationAtStation:at_station];
        
        if ( !self.last_notified || [self.last_notified timeIntervalSinceNow] < -180  ) {
            NSLog(@"%s notifying", __PRETTY_FUNCTION__);
            //UILocalNotification *notify = [[UILocalNotification alloc] init];
            //notify.alertBody = [NSString stringWithFormat:@"At %@", at_station.name];
            //[[UIApplication sharedApplication] presentLocalNotificationNow:notify];
            self.last_notified = [NSDate date];
        } else {
            NSLog(@"%s throttling notifications %@ %f", __PRETTY_FUNCTION__,
                  self.last_notified, [self.last_notified timeIntervalSinceNow]);
        }
    }
    else if ( (stations = [self betweenStations]) )
    {
        [self.delegate septaLineLocationBetweenStations:stations[0] and:stations[1]];
    }
    else
    {
        [self.delegate septaLineLocationClosestStation:[self closestStation]];
    }
}

#pragma mark - Geofencing Delegate Methods

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    NSLog(@"%s %@", __PRETTY_FUNCTION__, region);
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    UILocalNotification *notify = [[UILocalNotification alloc] init];
    notify.alertBody = [NSString stringWithFormat:@"Arriving %@", region.identifier];
    notify.soundName = @"silence.aif";
    [[UIApplication sharedApplication] presentLocalNotificationNow:notify];
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    NSLog(@"%s %@", __PRETTY_FUNCTION__, region);
    //UILocalNotification *notify = [[UILocalNotification alloc] init];
    //notify.alertBody = [NSString stringWithFormat:@"Departing %@", region.identifier];
    //[[UIApplication sharedApplication] presentLocalNotificationNow:notify];
}
- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    NSLog(@"%s %@", __PRETTY_FUNCTION__, region);
    // UILocalNotification *notify = [[UILocalNotification alloc] init];
    // notify.alertBody = [NSString stringWithFormat:@"Monitoring %@", region.identifier];
    // [[UIApplication sharedApplication] presentLocalNotificationNow:notify];
}

- (void)locationManager:(CLLocationManager *)manager
      didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    NSLog(@"%s %ld %@", __PRETTY_FUNCTION__, state, region);
}

@end