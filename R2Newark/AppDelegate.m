//
//  AppDelegate.m
//  R2Newark
//
//  Created by Xcode on 1/30/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

#import "AppDelegate.h"
@import AudioToolbox;

#import "AFNetworking/AFNetworking.h"
#import "SeptaData.h"
#import "SeptaStation.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSLog(@"%s Build: %@", __PRETTY_FUNCTION__, [[NSBundle mainBundle] infoDictionary][@"CFBundleLongVersionString"]);
    NSDate *currentDateTimeWithOffset = [NSDate dateWithTimeIntervalSinceNow:[[NSTimeZone localTimeZone] secondsFromGMT]];
    NSLog(@"%s %@", __PRETTY_FUNCTION__, currentDateTimeWithOffset);
    
    //NSLog(@"%s backgroundRefreshStatus: %d", __PRETTY_FUNCTION__, application.backgroundRefreshStatus);
    
    if ([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusAvailable) {
        NSLog(@"%s Background updates are available for the app.", __PRETTY_FUNCTION__);
    }else if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusDenied)
    {
        NSLog(@"%s The user explicitly disabled background behavior for this app or for the whole system.", __PRETTY_FUNCTION__);
    }else if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusRestricted)
    {
        NSLog(@"%s Background updates are unavailable and the user cannot enable them again. For example, this status can occur when parental controls are in effect for the current user.", __PRETTY_FUNCTION__);
    }
    
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    
    return YES;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    NSLog(@"%s %@", __PRETTY_FUNCTION__, notification.alertBody);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
