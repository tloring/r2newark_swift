//
//  SeptaStation.h
//  R2Newark
//
//  Created by Xcode on 2/1/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

@import Foundation;
@import CoreLocation;

@interface SeptaStation : NSObject

@property (nonatomic) NSInteger index;
@property (nonatomic) NSString *name;

@property (nonatomic) CLLocationDegrees latitude;
@property (nonatomic) CLLocationDegrees longitude;
@property (nonatomic) CLLocation *location;        // station location
@property (nonatomic) CLLocationCoordinate2D centerCoordinate;
@property (nonatomic) CLLocationDistance regionRadius;
@property (nonatomic) CLCircularRegion *region;

@property (nonatomic) CLLocationDistance distance; // from current location
@property (nonatomic) CLLocationDistance milesNextStationNorth;
@property (nonatomic) CLLocationDistance milesNextStationSouth;

@property (nonatomic) NSNumber *tminus;
@property (nonatomic) NSNumber *tplus;

- (instancetype)initWithName:(NSString *)name
                    latitude:(CLLocationDegrees)latitude
                   longitude:(CLLocationDegrees)longitude;

@end
