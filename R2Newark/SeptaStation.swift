//
//  SeptaStation.swift
//  R2Newark
//
//  Created by Thom Loring on 6/3/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

import CoreLocation

class SeptaStation: NSObject
{
    var index: NSInteger
    var name: NSString
    
    var latitude: CLLocationDegrees
    var longitude: CLLocationDegrees
    var location: CLLocation
    var centerCoordinate: CLLocationCoordinate2D
    var regionRadius: CLLocationDistance
    var region: CLCircularRegion
    
    var distance: CLLocationDistance // from current location
    var milesNextStationNorth: CLLocationDistance = 0
    var milesNextStationSouth: CLLocationDistance = 0
    
    var tminus: NSNumber = 0
    var tplus: NSNumber = 0

    init(name:String, latitude:CLLocationDegrees, longitude:CLLocationDegrees)
    {
        NSLog("@@ \(name), (\(latitude),\(longitude))")
        self.index = 0
        self.distance = 0
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.location = CLLocation(latitude:self.latitude, longitude:self.longitude)
        self.centerCoordinate = CLLocationCoordinate2DMake(latitude, longitude)
        self.regionRadius = 10.0
        self.region = CLCircularRegion(center:self.centerCoordinate, radius:self.regionRadius, identifier:self.name)
    }

    func description() -> String
    {
        return "!! \(self.name) at \(self.latitude),\(self.longitude) \(self.location)"
    }
}