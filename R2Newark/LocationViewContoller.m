//
//  LocationViewContoller.m
//  R2Newark
//
//  Created by Thom Loring on 2/17/14.
//  Copyright (c) 2014 Xcode. All rights reserved.
//

#import "LocationViewContoller.h"
#import "SeptaLine.h"
#import "SeptaStation.h"
#import "LineView.h"

@interface LocationViewContoller () <SeptaLineDelegate>

@property (strong, nonatomic) SeptaLine *septaLine;

// Line Control
@property (weak, nonatomic) IBOutlet LineView *lineControl;
@property (weak, nonatomic) IBOutlet UILabel *northStationLabel;
@property (weak, nonatomic) IBOutlet UILabel *southStationLabel;

// Current Location
@property (weak, nonatomic) IBOutlet UILabel *coordinates;
@property (weak, nonatomic) IBOutlet UILabel *accuracy;
@property (weak, nonatomic) IBOutlet UILabel *speed;
@property (weak, nonatomic) IBOutlet UILabel *course;
@property (weak, nonatomic) IBOutlet UILabel *timestamp;

// Current Context
@property (weak, nonatomic) IBOutlet UILabel *contextLabel;
@property (weak, nonatomic) IBOutlet UILabel *stationA;
@property (weak, nonatomic) IBOutlet UILabel *stationB;
@property (weak, nonatomic) IBOutlet UILabel *percentLabel;

// Build Info
@property (weak, nonatomic) IBOutlet UILabel *buildLabel;

@end

@implementation LocationViewContoller

- (void)viewDidLoad
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    // SeptaLine
    self.septaLine = [[SeptaLine alloc] initWithHomeStation:@"Claymont" cityStation:@"30th Street Station"];
    self.septaLine.delegate = self;
    
    // LineControl
    self.lineControl.total_distance = self.septaLine.total_distance;
    self.lineControl.mile_markers = self.septaLine.mile_markers;
    self.lineControl.northbound = YES;
    
    self.buildLabel.text = [[NSBundle mainBundle] infoDictionary][@"CFBundleLongVersionString"];
}

- (void)didReceiveMemoryWarning
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [super didReceiveMemoryWarning];
}

#pragma mark - SeptaLine Delegates

-(void)septaLineLocationUpdate:(SeptaLine *)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    CLLocation *location = sender.location;
    
    self.coordinates.text = [NSString stringWithFormat:@"(%.3f,%.3f)",
                             location.coordinate.latitude, location.coordinate.longitude];
    
    self.accuracy.text = [NSString stringWithFormat:@"(%.1f,%.1f)",
                          location.horizontalAccuracy, location.verticalAccuracy];
    
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"MM-dd-yyyy HH:mm:ss"];
    NSString *dateString = [dateFormater stringFromDate:sender.timestamp];

    
    self.timestamp.text = [NSString stringWithFormat:@"%@", dateString];
    self.speed.text = [NSString stringWithFormat:@"%.2f mph", sender.speed_mph];
    self.course.text = [NSString stringWithFormat:@"%@", sender.direction_string];
    
    self.northStationLabel.text = [NSString stringWithFormat:@"%.2f miles", [self.septaLine milesFrom:self.septaLine.cityStation]];
    self.southStationLabel.text = [NSString stringWithFormat:@"%.2f miles", [self.septaLine milesFrom:self.septaLine.homeStation]];
    
    // TODO: Move this w/i LineControl
    double north_distance = [self.septaLine milesFrom:self.septaLine.homeStation];
    double south_distance = [self.septaLine milesFrom:self.septaLine.cityStation];
    double total_distance = north_distance + south_distance;
    NSLog(@"%s north_distance: %f", __PRETTY_FUNCTION__, north_distance);
    NSLog(@"%s south_distance: %f", __PRETTY_FUNCTION__, south_distance);
    NSLog(@"%s total_distance: %f", __PRETTY_FUNCTION__, total_distance);
    
    NSLog(@"%s northbound: %i", __PRETTY_FUNCTION__, self.lineControl.northbound);
    if (self.lineControl.northbound) {
        self.lineControl.progress = north_distance/total_distance;
    } else {
        self.lineControl.progress = south_distance/total_distance;
    }
    // TODO
    
    self.percentLabel.text = [NSString stringWithFormat:@"%.2f%%", self.lineControl.progress * 100];
}

- (void)septaLineLocationAtStation:(SeptaStation *)station
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    self.contextLabel.text = @"At Station";
    self.stationA.text = station.name;
    self.stationB.hidden = YES;
}

- (void)septaLineLocationBetweenStations:(SeptaStation *)stationA and:(SeptaStation *)stationB
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    self.contextLabel.text = @"Between Stations";
    self.stationA.text = [NSString stringWithFormat:@"%@ (%.2f miles)",
                          [stationA name], [self.septaLine milesFrom:stationA]];
    self.stationB.hidden = NO;
    self.stationB.text = [NSString stringWithFormat:@"%@ (%.2f miles)",
                          [stationB name], [self.septaLine milesFrom:stationB]];
}

- (void)septaLineLocationClosestStation:(SeptaStation *)station
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    self.contextLabel.text = @"Closest Station";
    self.stationA.text = [NSString stringWithFormat:@"%@ (%.2f miles)",
                          station.name, [self.septaLine milesFrom:station]];
    self.stationB.hidden = YES;
}

@end